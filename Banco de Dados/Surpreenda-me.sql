USE surpreendame;

	CREATE TABLE Voluntários (
    
    Codigo integer unsigned not null auto_increment,
    Nome VARCHAR(40) not null,
    Email VARCHAR(30) not null,
    Telefone VARCHAR(12) not null,
    Celular VARCHAR(15) not null,
    Data_Nascimento DATE not null,
    PRIMARY KEY (Codigo)
    
);

	CREATE TABLE Alunos (
    
    Codigo integer unsigned not null auto_increment,
    Nome VARCHAR(40) not null,
    Email VARCHAR(30) not null,
    Telefone VARCHAR(12) not null,
    Celular VARCHAR(15) not null,
    Data_Nascimento DATE not null,
    Idade INTEGER unsigned not null,
    PRIMARY KEY (Codigo)
    
    );
    
    CREATE TABLE Projetos (
    
    Codigo INTEGER unsigned not null auto_increment,
    Curso VARCHAR(40) NOT NULL,
    Tipo VARCHAR(40) NOT NULL,
    PRIMARY KEY (Codigo)
    
    );
    
    CREATE TABLE Patrocinador (
    
    Codigo INTEGER unsigned not null auto_increment,
    Nome VARCHAR(40) not null,
    Email VARCHAR(30) not null,
    Telefone VARCHAR(12) not null,
    Celular VARCHAR(15) not null,
    Tipo INTEGER unsigned not null,
    PRIMARY KEY (Codigo),
    INDEX fk_Tipo (Tipo),
    FOREIGN KEY (Tipo) REFERENCES Forma (Codigo)
   
    );
    
    CREATE TABLE Forma (
    
    Codigo INTEGER unsigned not null auto_increment,
    Nome VARCHAR(50) not null,
    PRIMARY KEY (Codigo)
    
    );
    
    CREATE TABLE Seleção (
    
    Codigo INTEGER unsigned not null auto_increment,
    Motivo VARCHAR(1000) not null,
    Aluno INTEGER UNSIGNED NOT NULL,
    PRIMARY KEY (Codigo),
    INDEX fk_Aluno (Aluno),
    FOREIGN KEY (Aluno) REFERENCES Alunos (Codigo)
    
    );
    
    DROP TABLE Patrocinador;
    
    CREATE TABLE Patrocinador (
    
    Codigo INTEGER unsigned not null auto_increment,
    Nome VARCHAR(40) not null,
    Email VARCHAR(30) not null,
    Telefone VARCHAR(12) not null,
    Celular VARCHAR(15) not null,
    Tipo INTEGER unsigned not null,
    PRIMARY KEY (Codigo),
    INDEX fk_Tipo (Tipo),
    FOREIGN KEY (Tipo) REFERENCES Forma (Codigo)
   
    );
    
    DROP TABLE Seleção;
    
    CREATE TABLE Seleção (
    
    Codigo INTEGER unsigned not null auto_increment,
    Motivo VARCHAR(1000) not null,
    Aluno INTEGER UNSIGNED NOT NULL,
    PRIMARY KEY (Codigo),
    INDEX fk_Aluno (Aluno),
    FOREIGN KEY (Aluno) REFERENCES Alunos (Codigo)
    
    );
    
    
    
    
    