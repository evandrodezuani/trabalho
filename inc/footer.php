 <footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Bio</h5>
          <p class="grey-text text-lighten-4"> 
          Somos uma instituição que visa ajudar da melhor forma possível crianças, idosos, animais. </p>


        </div>
        <!-- div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div> -->
 
          <div class="fixed-action-btn direction-top active" style="bottom: 45px; right: 24px;">
             
             <a id="menu" onclick="$('.tap-target').tapTarget('open')" class="btn btn-floating btn-large teal" href="#">clicar
               <i class="material-icons">
                 <font style="vertical-align: inherit;">
                   <font style="vertical-align: inherit;"></font>
                 </font>
               </i>
             </a>

           </div>

           <div class="tap-target teal" data-target="menu">
    <div class="tap-target-content ">
      <h5 class="float left">Nossas Redes Sociais</h5>
      <p></p>
      
      <ul>
   <li class="facebook"><a href="#" title="Curta nossa página no Facebook"></a>
    <i style="font-size:24px; margin-left: 20px;" class="fa">&#xf230;</i> </li>
   <li class="twitter"><a href="#" title="Siga-nos no Twitter"></a>
   <i style="font-size:24px; margin-left: 20px;" class="fa">&#xf099;</i></li>
   <li class="instagram"><a href="#" title="Siga-nos no Instagram"></a>
   <i style="font-size:24px; margin-left: 20px;" class="fa">&#xf16d;</i></li>
   <li class="whatsapp"><a href="#" title="Mande sua mensagem no nosso WhatsApp"></a>
   <i style="font-size:24px; margin-left: 20px " class="fa">&#xf232;</i></li>
      
  
</ul>


    </div>
  </div>

        <div class="col l3 s12">
          <h5 class="white-text">Fale Conosco</h5>
          <ul>
            <li><a class="white-text" href="#">Facebook</a>  <i style="font-size:24px;" class="fa">&#xf230;</i></li>
            <li><a class="white-text" href="#">WhatsApp</a>  <i style="font-size:24px;" class="fa">&#xf232;</i></li>
            <li><a class="white-text" href="#">Instagram</a> <i style="font-size:24px;" class="fa">&#xf16d;</i></li>
            <li><a class="white-text" href="#">Twitter</a>   <i style="font-size:24px;" class="fa">&#xf099;</i></li>
          </ul>
       
      </div>
    </div>
     <div class="footer-copyright">
      <div class="container">  <a class="brown-text text-lighten-3"></a>
      </div>
    </div> 
  </footer>